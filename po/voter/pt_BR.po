#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-12-03 08:51+0000\n"
"PO-Revision-Date: 2021-05-02 06:31+0000\n"
"Last-Translator: Diego Silva <diego.cs282@gmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://hosted.weblate.org/projects/"
"belenios/voter/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.7-dev\n"

#: src/web/server/common/pages_common.ml:370
msgid " (if you forgot it, for example)."
msgstr " (se você esquecê-la, por exemplo)."

#: src/web/server/common/pages_voter.ml:875
msgid " A confirmation e-mail has been sent to you."
msgstr " Um e-mail de confirmação foi enviado para você."

#: src/web/server/common/pages_voter.ml:649
msgid " and "
msgstr " e "

#: src/web/server/common/pages_voter.ml:874
msgid " anytime during the election."
msgstr " em qualquer momento durante a eleição."

#: src/web/server/common/pages_voter.ml:939
msgid " ballot(s) have been accepted so far."
msgstr " cédula(s) que foram aceitas até o momento."

#: src/web/server/common/pages_voter.ml:949
msgid " ballot(s) have been accepted, and "
msgstr " cédula(s) que foram aceitas, e "

#: src/web/server/common/pages_voter.ml:944
msgid " ballot(s) have been accepted."
msgstr " cédula(s) que foram aceitas."

#: src/web/server/common/web_common.ml:96
msgid " day(s)"
msgstr " dia(s)"

#: src/web/server/common/pages_voter.ml:160
msgid " for more information."
msgstr " para maiores informações."

#: src/web/server/common/pages_voter.ml:864
msgid " has been accepted."
msgstr " foi aceito(a)."

#: src/web/server/common/pages_voter.ml:743
msgid " has been received, but not recorded yet. "
msgstr " foi recebido, mas não foi registrado ainda. "

#: src/web/server/common/pages_voter.ml:951
msgid " have been tallied."
msgstr " foram apurados."

#: src/web/server/common/web_common.ml:98
msgid " hour(s)"
msgstr " hora(s)"

#: src/web/server/common/pages_common.ml:502
msgid " in the following box: "
msgstr " na caixa seguinte "

#: src/web/server/common/pages_voter.ml:878
msgid " is rejected, because "
msgstr " foi rejeitado, porque "

#: src/web/server/common/web_common.ml:99
msgid " minute(s)"
msgstr " minuto(s)"

#: src/web/server/common/web_common.ml:95
msgid " month(s)"
msgstr " mês(es)"

#: src/web/server/common/web_common.ml:100
msgid " second(s)"
msgstr " segundo(s)"

#: src/web/server/common/web_common.ml:94
msgid " year(s)"
msgstr " ano(s)"

#: src/web/server/common/pages_voter.ml:1002
msgid "!"
msgstr "!"

#: src/web/server/common/pages_voter.ml:129
#. src/web/server/common/pages_voter.ml:103
msgid "%d blank ballot(s)"
msgstr ""

#: src/web/server/common/pages_voter.ml:1239
#. src/web/server/common/pages_voter.ml:107
msgid "%d invalid ballot(s)"
msgstr "%d cédula(s) inválida(s)"

#: src/web/server/common/pages_voter.ml:494
msgid "%d of the following %d trustees (verification keys) [public keys] are needed to decrypt the election result:"
msgstr ""
"%d das seguintes %d autoridades certificadoras (chaves verificadoras) ["
"chaves públicas] são necessárias para descriptografar o resultado da eleição:"

#: src/web/server/common/pages_voter.ml:123
#. src/web/server/common/pages_voter.ml:97
msgid "%d valid (non-blank) ballot(s)"
msgstr ""

#: src/web/server/common/pages_voter.ml:124
#. src/web/server/common/pages_voter.ml:98
msgid "%d valid ballot(s)"
msgstr ""

#: src/web/clients/tool/tool_js_booth.ml:168
msgid "(nothing)"
msgstr "(nada)"

#: src/web/server/common/pages_common.ml:368
msgid ", or "
msgstr ", ou "

#: src/web/server/common/pages_voter.ml:1143
msgid "1 is the highest grade, 2 is the second highest grade, etc."
msgstr "1 é a nota mais alta, 2 é a segunda nota mais alta, etc."

#: src/web/server/common/pages_voter.ml:154
msgid "A Condorcet winner is a candidate that is preferred over all the other candidates."
msgstr ""
"Um ganhador de Condorcet é um candidato que teve a preferência sobre todos "
"os outros candidatos."

#: src/web/server/common/pages_voter.ml:1245
msgid "A ballot is invalid if two candidates have been given the same preference order or if a rank is missing."
msgstr ""
"Uma cédula é inválida se dois candidatos receberam a mesma ordem de "
"preferência ou se falta uma classificação."

#: src/web/server/common/pages_voter.ml:411
msgid "Accept"
msgstr "Aceitar"

#: src/web/server/common/pages_voter.ml:914
msgid "Accepted ballots"
msgstr "Cédulas aceitas"

#: src/web/server/common/pages_voter.ml:111
msgid "According to Majority Judgment, the ranking is:"
msgstr ""

#: src/web/server/common/pages_common.ml:155
#. src/web/server/common/pages_common.ml:84
msgid "Administer elections"
msgstr "Administrar eleições"

#: src/web/server/common/pages_common.ml:157
#. src/web/server/common/pages_common.ml:86
msgid "Administer this election"
msgstr "Administrar esta eleição"

#: src/web/server/common/pages_voter.ml:340
msgid "Advanced mode"
msgstr "Modo avançado"

#: src/web/server/common/pages_voter.ml:470
msgid "All of the following trustees (verification keys) are needed to decrypt the result:"
msgstr ""
"As seguintes autoridades (chaves verificadoras) são necessárias para "
"descriptografar o resultado:"

#: src/web/server/common/pages_voter.ml:1027
#. src/web/server/common/pages_voter.ml:820
#. src/web/server/common/pages_voter.ml:818
#. src/web/server/common/pages_voter.ml:686
#. src/web/server/common/pages_voter.ml:684
msgid "Answer to questions"
msgstr "Responder às questões"

#: src/web/server/common/pages_voter.ml:1145
msgid "As a convenience, 0 is always interpreted as the lowest grade."
msgstr "Por conveniência, 0 é sempre interpretado como a nota mais baixa."

#: src/web/clients/tool/tool_js_booth.ml:255
msgid "At least one of the answers is invalid!"
msgstr "Ao menos uma das respostas é inválida!"

#: src/web/server/common/pages_voter.ml:55
msgid "Audit data: "
msgstr "Dados para auditoria: "

#: src/web/server/common/pages_voter.ml:1031
#. src/web/server/common/pages_voter.ml:838
#. src/web/server/common/pages_voter.ml:836
#. src/web/server/common/pages_voter.ml:704
#. src/web/server/common/pages_voter.ml:702
#. src/web/server/common/pages_common.ml:443
#. src/web/server/common/pages_common.ml:397
#. src/web/server/common/pages_common.ml:348
msgid "Authenticate"
msgstr "Autenticar"

#: src/web/server/common/pages_common.ml:306
msgid "Authenticate with %s"
msgstr ""

#: src/web/server/common/pages_common.ml:408
msgid "Authentication failed"
msgstr "Falha na autenticação"

#: src/web/server/common/pages_common.ml:411
msgid "Authentication failed, probably because of a bad username or password, or you are not allowed to perform this operation."
msgstr ""
"Falha na autenticação, provavelmente por um nome de usuário ou senha "
"errados, ou você não tem permissão para fazer essa operação."

#: src/web/server/common/pages_voter.ml:222
msgid "Available methods on this server:"
msgstr "Métodos disponíveis nesse servidor:"

#: src/web/server/common/pages_common.ml:481
msgid "Bad e-mail address!"
msgstr "Endereço de e-mail ruim!"

#: src/web/server/common/pages_common.ml:480
msgid "Bad security code!"
msgstr "Código de segurança incorreto!"

#: src/web/server/common/pages_voter.ml:976
#. src/web/server/common/pages_voter.ml:964
msgid "Belenios Booth"
msgstr "Urna Belenios"

#: src/web/server/common/pages_common.ml:465
msgid "Belenios Server"
msgstr "Servidor Belenios"

#: src/web/server/common/pages_common.ml:467
msgid "Belenios authentication"
msgstr ""

#: src/web/server/common/pages_common.ml:461
msgid "Best regards,"
msgstr "Atenciosamente,"

#: src/web/clients/tool/tool_js_booth.ml:160
#. src/web/clients/tool/tool_js_booth.ml:86
#. src/web/server/common/pages_voter.ml:178
msgid "Blank vote"
msgstr "Voto em branco"

#: src/web/server/common/pages_voter.ml:408
msgid "By using this site, you accept our "
msgstr "Ao utilizar este site, você aceita nossas "

#: src/web/clients/tool/tool_js_fingerprint.ml:84
msgid "Compare"
msgstr "Comparar"

#: src/web/clients/tool/tool_js_fingerprint.ml:68
msgid "Compute fingerprint"
msgstr "Computar assinatura"

#: src/web/clients/tool/tool_js_fingerprint.ml:35
msgid "Computed fingerprint:"
msgstr "Assinatura computada:"

#: src/web/server/common/pages_voter.ml:1118
msgid "Condorcet-Schulze method"
msgstr "Método Condorcet-Schulze"

#: src/web/server/common/pages_voter.ml:1033
#. src/web/server/common/pages_voter.ml:847
#. src/web/server/common/pages_voter.ml:845
#. src/web/server/common/pages_voter.ml:713
#. src/web/server/common/pages_voter.ml:711
msgid "Confirm"
msgstr "Confirmação"

#: src/web/server/common/pages_voter.ml:1191
#. src/web/server/common/pages_voter.ml:1134
#. src/web/server/common/pages_voter.ml:1013
msgid "Continue"
msgstr "Continuar"

#: src/web/server/common/site_voter.ml:115
msgid "Cookies are blocked"
msgstr "Os cookies estão bloqueados"

#: src/web/server/common/mails_voter.ml:127
msgid "Credential:"
msgstr "Credencial:"

#: src/web/server/common/pages_voter.ml:504
msgid "Credentials were generated and sent by %s and have fingerprint %s."
msgstr ""
"As credenciais foram geradas e enviadas por %s e tem a identificação %s."

#: src/web/server/common/pages_common.ml:452
#. src/web/server/common/mails_voter.ml:359
msgid "Dear %s,"
msgstr "Prezado(a) %s,"

#: src/web/server/common/pages_voter.ml:1035
msgid "Done"
msgstr "Terminado"

#: src/web/server/common/pages_common.ml:493
#. src/web/server/common/pages_common.ml:377
#. src/web/server/common/pages_common.ml:332
msgid "E-mail address:"
msgstr "Endereço de e-mail:"

#: src/web/server/common/pages_voter.ml:1095
msgid "Election UUID: "
msgstr "UUID da eleição: "

#: src/web/server/common/pages_voter.ml:1099
#. src/web/server/common/pages_voter.ml:51
msgid "Election fingerprint: "
msgstr "Identificador da eleição: "

#: src/web/server/common/pages_voter.ml:1078
#. src/web/server/common/pages_common.ml:183
#. src/web/server/common/pages_common.ml:115
msgid "Election server"
msgstr "Servidor de eleições"

#: src/web/server/common/pages_voter.ml:1065
msgid "Encrypting…"
msgstr "Criptografando…"

#: src/web/server/common/site_voter.ml:359
#. src/web/server/common/site_voter.ml:334
#. src/web/server/common/site_voter.ml:286
#. src/web/server/common/site_voter.ml:281
#. src/web/server/common/site_voter.ml:276
msgid "Error"
msgstr "Erro"

#: src/web/clients/tool/tool_js_fingerprint.ml:75
msgid "Expected fingerprint:"
msgstr "Assinatura esperada:"

#: src/web/server/common/pages_voter.ml:881
msgid "FAIL!"
msgstr "FALHA!"

#: src/web/server/common/site_common.ml:110
msgid "Forbidden"
msgstr ""

#: src/web/server/common/pages_common.ml:209
#. src/web/server/common/pages_common.ml:138
msgid "Get the source code"
msgstr "Obter o código fonte"

#: src/web/server/common/pages_voter.ml:932
#. src/web/server/common/pages_voter.ml:895
#. src/web/server/common/pages_voter.ml:792
#. src/web/server/common/pages_voter.ml:762
msgid "Go back to election"
msgstr "Voltar para a eleição"

#: src/web/server/common/pages_voter.ml:647
msgid "I am "
msgstr "Eu sou "

#: src/web/server/common/pages_voter.ml:652
msgid "I cast my vote"
msgstr "Eu voto"

#: src/web/clients/tool/tool_js_booth.ml:233
msgid "If you are asked to grade candidates (majority judgement) then 1 is the best grade, higher numbers are worse."
msgstr ""
"Se for solicitado a você classificar os candidatos (voto majoritário) então "
"1 é a melhor classificação, números mais altos são piores."

#: src/web/clients/tool/tool_js_booth.ml:236
msgid "If you are asked to rank candidates (Condorcet, STV, ...) then use 1 for your first choice, 2 for the second, etc."
msgstr ""
"Se for solicitado a você classificar os candidatos (Condorcet, STV, ...) "
"então use 1 para sua primeira escolha, 2 para a segunda, etc."

#: src/web/server/common/pages_voter.ml:786
msgid "If you want to vote, you must "
msgstr "Se você quer votar, você deve "

#: src/web/server/common/pages_voter.ml:1269
msgid "In our implementation, when several candidates have the same number of votes when they are ready to be elected or eliminated, we follow the order in which candidates were listed in the election."
msgstr ""
"Em nossa implementação, quando vários candidatos tem o mesmo número de votos "
"quando estão prontos para serem eleitos ou eliminados, nós seguimos a ordem "
"em que os candidatos foram incluídos na eleição."

#: src/web/server/common/pages_voter.ml:1141
msgid "In the context of Majority Judgment, a vote gives a grade to each candidate."
msgstr "No contexto do Voto Majoritário, um voto dá uma nota a cada candidato."

#: src/web/server/common/pages_voter.ml:1198
msgid "In the context of STV, voters rank candidates by order of preference."
msgstr ""
"No contexto do STV, eleitores classificam os candidatos em ordem de "
"preferência."

#: src/web/server/common/pages_voter.ml:1025
#. src/web/server/common/pages_voter.ml:811
#. src/web/server/common/pages_voter.ml:809
#. src/web/server/common/pages_voter.ml:677
#. src/web/server/common/pages_voter.ml:675
msgid "Input credential"
msgstr "Inserir credencial"

#: src/web/server/common/pages_voter.ml:1044
msgid "Input your credential "
msgstr "Insira sua credencial "

#: src/web/clients/tool/tool_js_booth.ml:386
msgid "Invalid credential!"
msgstr "Credencial inválida!"

#: src/web/server/common/site_voter.ml:287
msgid "Invalid index for question."
msgstr "Número da pergunta inválido."

#: src/web/server/common/pages_voter.ml:240
msgid "It contains all submitted ballots in clear, in random order."
msgstr ""

#: src/web/server/common/pages_voter.ml:220
msgid "It is up to you to apply your favorite counting method."
msgstr ""

#: src/web/server/common/pages_voter.ml:263
msgid "It will open in "
msgstr "Abrirá em "

#: src/web/server/common/pages_voter.ml:238
msgid "JSON result"
msgstr "resultado JSON"

#: src/web/server/common/pages_common.ml:231
msgid "Language:"
msgstr "Idioma:"

#: src/web/server/common/pages_voter.ml:971
msgid "Loading…"
msgstr "Carregando…"

#: src/web/server/common/pages_common.ml:442
#. src/web/server/common/pages_common.ml:396
#. src/web/server/common/pages_common.ml:347
#. src/web/server/common/pages_common.ml:324
msgid "Log in"
msgstr "Entrar"

#: src/web/server/common/pages_common.ml:305
msgid "Log in with %s"
msgstr "Entre com %s"

#: src/web/server/common/pages_voter.ml:1273
msgid "Look at the raw events for more details."
msgstr "Olhe os eventos brutos para maiores detalhes."

#: src/web/server/common/pages_voter.ml:226
msgid "Majority Judgment"
msgstr "Voto majoritário"

#: src/web/server/common/pages_voter.ml:1175
#. src/web/server/common/pages_voter.ml:1125
msgid "Majority Judgment method"
msgstr "Método de Voto Majoritário"

#: src/web/server/common/pages_voter.ml:1267
msgid "Many variants of STV exist, depending for example on how to break ties."
msgstr ""
"Existem muitas variações de STV, dependendo por exemplo de como dividir os "
"grupos."

#: src/web/server/common/pages_voter.ml:1205
msgid "Many variants of STV exist, we documented our choices in "
msgstr "Existem muitas variações de STV, nós documentamos nossas escolhas em "

#: src/web/server/common/pages_voter.ml:1202
#. src/web/server/common/pages_voter.ml:1149
msgid "More information can be found "
msgstr "Maiores informações podem ser encontradas "

#: src/web/clients/tool/tool_js_booth.ml:317
msgid "Next"
msgstr "Próximo"

#: src/web/clients/tool/tool_js_booth.ml:140
msgid "No other choices are allowed when voting blank"
msgstr "Nenhuma outra opção é permitida quando se vota em branco"

#: src/web/server/common/site_common.ml:52
msgid "Not found"
msgstr "Não encontrado"

#: src/web/server/common/mails_voter.ml:53
msgid "Note that you also need a credential, sent in a separate email, to start voting."
msgstr ""
"Note que você também precisa de uma credencial, enviada em um email "
"separado, para votar."

#: src/web/server/common/pages_voter.ml:667
msgid "Note: You have already voted. Your vote will be replaced."
msgstr "Nota: Você já votou. Seu voto será substituído."

#: src/web/server/common/pages_voter.ml:753
msgid "Note: your ballot is encrypted and nobody can see its contents."
msgstr ""
"Nota: sua cédula está criptografada e ninguém poderá ver o seu conteúdo."

#: src/web/clients/tool/tool_js_booth.ml:228
msgid "Notes:"
msgstr "Notas:"

#: src/web/server/common/pages_voter.ml:376
msgid "Number of accepted ballots: "
msgstr "Número de cédulas aceitas: "

#: src/web/server/common/pages_voter.ml:1131
msgid "Number of grades:"
msgstr "Número de classificações:"

#: src/web/server/common/pages_voter.ml:1188
msgid "Number of seats:"
msgstr "Números de lugares:"

#: src/web/server/common/mails_voter.ml:132
#. src/web/server/common/mails_voter.ml:61
msgid "Number of votes:"
msgstr "Número de votos:"

#: src/web/server/common/mails_voter.ml:138
#. src/web/server/common/mails_voter.ml:67
msgid "Only the last vote counts."
msgstr "Somente o último voto conta."

#: src/web/server/common/mails_voter.ml:135
#. src/web/server/common/mails_voter.ml:64
msgid "Page of the election:"
msgstr "Página da eleição:"

#: src/web/server/common/pages_common.ml:389
#. src/web/server/common/mails_voter.ml:57
msgid "Password:"
msgstr "Senha:"

#: src/web/server/common/pages_common.ml:500
msgid "Please enter "
msgstr "Por favor, envie "

#: src/web/server/common/pages_common.ml:429
msgid "Please enter the verification code received by e-mail:"
msgstr "Insira o código de verificação recebido por e-mail:"

#: src/web/clients/tool/tool_js_booth.ml:381
msgid "Please enter your credential:"
msgstr "Por favor, insira sua credencial:"

#: src/web/server/common/mails_voter.ml:49
msgid "Please find below your login and password for the election"
msgstr "Verifique abaixo o seu login e a sua senha para a eleição"

#: src/web/server/common/pages_voter.ml:658
msgid "Please log in to confirm your vote."
msgstr "Por favor entre para confirmar seu voto."

#: src/web/server/common/pages_common.ml:321
msgid "Please log in:"
msgstr "Por favor entre:"

#: src/web/clients/tool/tool_js_fingerprint.ml:58
msgid "Please paste the data for which you want to compute the fingerprint in the text area below:"
msgstr ""

#: src/web/server/common/pages_voter.ml:1159
msgid "Please provide the number of grades to see the result of the election according to the Majority Judgment method."
msgstr ""
"Por favor, providencie o número de classificações para ver o resultado da "
"eleição de acordo com o método do Voto Majoritário."

#: src/web/server/common/pages_voter.ml:1213
msgid "Please provide the number of seats to see the result of the election according to the Single Transferable Vote method."
msgstr ""
"Por favor providencie o número de lugares para ver o resultado da eleição de "
"acordo com o método Single Transferable Vote (STV) ou Voto Único "
"Transferível (VUT)."

#: src/web/server/common/pages_voter.ml:1064
msgid "Please wait while your ballot is being encrypted…"
msgstr "Por favor espere enquanto sua cédula está sendo criptografada…"

#: src/web/server/common/pages_voter.ml:970
msgid "Please wait… "
msgstr "Por favor, espere... "

#: src/web/server/common/pages_common.ml:204
#. src/web/server/common/pages_common.ml:133
msgid "Powered by "
msgstr "Distribuído por "

#: src/web/clients/tool/tool_js_booth.ml:316
msgid "Previous"
msgstr "Anterior"

#: src/web/server/common/pages_common.ml:211
#. src/web/server/common/pages_common.ml:140
msgid "Privacy policy"
msgstr "Política de privacidade"

#: src/web/server/common/pages_common.ml:279
msgid "Proceed"
msgstr "Continuar"

#: src/web/server/common/pages_voter.ml:1253
msgid "Raw events"
msgstr "Eventos brutos"

#: src/web/server/common/pages_voter.ml:1068
msgid "Restart"
msgstr "Reiniciar"

#: src/web/server/common/mails_voter.ml:384
msgid "Results will be published on the election page"
msgstr "Os resultados serão publicados na página da eleição"

#: src/web/server/common/pages_voter.ml:1029
#. src/web/server/common/pages_voter.ml:829
#. src/web/server/common/pages_voter.ml:827
#. src/web/server/common/pages_voter.ml:695
#. src/web/server/common/pages_voter.ml:693
msgid "Review and encrypt"
msgstr "Revisar e criptografar"

#: src/web/server/common/pages_voter.ml:1009
msgid "Save it to check that it is taken into account later."
msgstr "Salve-o para posteriormente verificar que seu voto foi considerado."

#: src/web/server/common/pages_voter.ml:312
msgid "See accepted ballots"
msgstr "Ver as cédulas aceitas"

#: src/web/clients/tool/tool_js_booth.ml:80
msgid "Select between %d and %d answer(s)"
msgstr "Selecione entre %d e %d resposta(s)"

#: src/web/server/common/pages_common.ml:234
msgid "Set"
msgstr "Confirmar"

#: src/web/server/common/pages_voter.ml:156
msgid "Several techniques exist to decide which candidate to elect when there is no Condorcet winner."
msgstr ""
"Existem diversas técnicas para decidir quando um candidato é eleito quando "
"não há um vencedor por Condorcet."

#: src/web/server/common/pages_voter.ml:228
msgid "Single Transferable Vote"
msgstr "Voto Único Tranferível"

#: src/web/server/common/pages_voter.ml:1229
#. src/web/server/common/pages_voter.ml:1182
msgid "Single Transferable Vote method"
msgstr "Método de Voto Único Transferível"

#: src/web/server/common/pages_voter.ml:333
msgid "Start"
msgstr "Iniciar"

#: src/web/server/common/pages_voter.ml:812
#. src/web/server/common/pages_voter.ml:678
msgid "Step 1"
msgstr ""

#: src/web/server/common/pages_voter.ml:1040
msgid "Step 1/6: Input credential"
msgstr "Passo 1/6: Insira sua credencial"

#: src/web/server/common/pages_voter.ml:821
#. src/web/server/common/pages_voter.ml:687
msgid "Step 2"
msgstr ""

#: src/web/server/common/pages_voter.ml:1050
msgid "Step 2/6: Answer to questions"
msgstr "Passo 2/6: Responda as questões"

#: src/web/server/common/pages_voter.ml:830
#. src/web/server/common/pages_voter.ml:696
msgid "Step 3"
msgstr ""

#: src/web/server/common/pages_voter.ml:1055
msgid "Step 3/6: Review and encrypt"
msgstr "Passo 3/6: Revisar e criptografar"

#: src/web/server/common/pages_voter.ml:839
#. src/web/server/common/pages_voter.ml:705
msgid "Step 4"
msgstr ""

#: src/web/server/common/pages_voter.ml:848
#. src/web/server/common/pages_voter.ml:714
msgid "Step 5"
msgstr ""

#: src/web/server/common/pages_common.ml:506
#. src/web/server/common/pages_common.ml:434
msgid "Submit"
msgstr "Enviar"

#: src/web/server/common/pages_voter.ml:1271
msgid "Such candidates are marked as \"TieWin\" when they are elected and as \"TieLose\" if they have lost."
msgstr ""
"Tal candidados são marcados como \"TieWin\" quando eles são eleitos e como "
"\"TieLose\" se eles perderam."

#: src/web/server/common/pages_voter.ml:876
msgid "Thank you for voting!"
msgstr "Obrigado por votar!"

#: src/web/server/common/pages_voter.ml:165
msgid "The Schulze winners are:"
msgstr "Os vencedores do método de Schulze são:"

#: src/web/server/common/pages_voter.ml:1280
msgid "The Single Transferable Vote winners are:"
msgstr "Os vencedores do Voto Único Transferível são:"

#: src/web/server/common/pages_voter.ml:294
#. src/web/server/common/pages_voter.ml:289
msgid "The election is closed and being tallied."
msgstr "A eleição está fechada e iniciando a contagem."

#: src/web/server/common/pages_voter.ml:279
msgid "The election will close in "
msgstr "A eleição será fechada em "

#: src/web/server/common/pages_voter.ml:524
msgid "The fingerprint of the encrypted tally is %s."
msgstr "A identificação da criptografia da contagem é %s."

#: src/web/clients/tool/tool_js_fingerprint.ml:47
msgid "The fingerprints differ!"
msgstr "As assinaturas estão diferentes!"

#: src/web/clients/tool/tool_js_fingerprint.ml:45
msgid "The fingerprints match!"
msgstr "As assinaturas conferem!"

#: src/web/server/common/pages_voter.ml:1157
msgid "The number of different grades (Excellent, Very Good, etc.) typically varies from 5 to 7."
msgstr ""
"O número de diferentes classificações (Excelente, Muito Bom, etc.) "
"tipicamente varia entre 5 e 7."

#: src/web/server/common/site_voter.ml:335
msgid "The number of grades is invalid."
msgstr "O número de classificação é inválido."

#: src/web/server/common/site_voter.ml:360
msgid "The number of seats is invalid."
msgstr "O número de lugares é inválido."

#: src/web/server/common/pages_voter.ml:237
msgid "The raw results can be viewed in the "
msgstr "O resultado bruto pode ser visto em "

#: src/web/server/common/pages_voter.ml:397
msgid "The result of this election is currently not publicly available. It will be in %s."
msgstr "O resultado da eleição atualmente não está disponível. Estará em %s."

#: src/web/server/common/site_voter.ml:277
msgid "The result of this election is not available."
msgstr "O resultado da eleição não está disponível."

#: src/web/server/common/pages_voter.ml:451
msgid "The total weight is %s (min: %s, max: %s)."
msgstr ""

#: src/web/server/common/pages_voter.ml:427
msgid "The voter list has %d voter(s) and fingerprint %s."
msgstr "A lista de eleitores tem %d eleitor(es) e identificação %s."

#: src/web/server/common/pages_voter.ml:1147
msgid "The winner is the candidate with the highest median (or the 2nd highest median if there is a tie, etc.)."
msgstr ""
"O vencedor é o candidato com a média mais alta (ou a segunda mais alta média "
"se houver um empate, etc.)."

#: src/web/server/common/pages_voter.ml:1265
msgid "There has been at least one tie."
msgstr "Houve pelo menos um empate."

#: src/web/server/common/site_common.ml:52
msgid "This election does not exist. This may happen for elections that have not yet been open or have been deleted."
msgstr ""
"Essa eleição não existe. Isso pode ter acontecido pela eleição ainda não "
"estar aberta ou por ter sido apagada."

#: src/web/server/common/pages_voter.ml:299
msgid "This election has been tallied."
msgstr "Essa eleição já foi apurada."

#: src/web/server/common/pages_voter.ml:420
msgid "This election is administered by %s."
msgstr "Essa eleição é administrada por %s."

#: src/web/server/common/pages_voter.ml:304
msgid "This election is archived."
msgstr "Essa eleição está arquivada."

#: src/web/server/common/pages_voter.ml:271
msgid "This election is currently closed."
msgstr "Essa eleição atualmente está fechada."

#: src/web/server/common/pages_voter.ml:440
msgid "This election uses weights!"
msgstr "Essa eleição utiliza pesos!"

#: src/web/clients/tool/tool_js_booth.ml:388
msgid "This looks like a password... Maybe you looked at the wrong e-mail?"
msgstr "Isso parece uma senha... Talvez você tenha visto o e-mail errado?"

#: src/web/server/common/site_voter.ml:282
msgid "This question is homomorphic, this method cannot be applied to its result."
msgstr ""
"Essa questão é homomórfica, esse método não pode ser aplicado ao resultado."

#: src/web/server/common/mails_voter.ml:376
msgid "This vote replaces any previous vote."
msgstr "Esse voto substitui o voto anterior."

#: src/web/server/common/pages_voter.ml:145
#. src/web/server/common/pages_voter.ml:89
msgid "Tie:"
msgstr "Empate:"

#: src/web/server/common/mails_voter.ml:123
msgid "To cast a vote, you will also need a password, sent in a separate email."
msgstr ""
"Para votar, você também precisará de uma senha, enviado em um e-mail "
"separado."

#: src/web/server/common/mails_voter.ml:40
msgid "To get more information, please contact:"
msgstr "Para maiores informações, por favor contate:"

#: src/web/server/common/pages_voter.ml:362
msgid "Total weight of accepted ballots:"
msgstr "Peso total dos votos aceitos:"

#: src/web/server/common/pages_voter.ml:514
msgid "Trustees shuffled the ballots in the following order:"
msgstr "Autoridades embaralharam as cédulas na seguinte ordem:"

#: src/web/server/common/pages_common.ml:455
msgid "Use the following code:"
msgstr "Utilize o seguinte código:"

#: src/web/server/common/pages_common.ml:376
#. src/web/server/common/pages_common.ml:331
#. src/web/server/common/mails_voter.ml:129
#. src/web/server/common/mails_voter.ml:56
msgid "Username:"
msgstr "Nome de usuário:"

#: src/web/clients/tool/tool_js_booth.ml:209
msgid "Value must be an integer between 0 and 255."
msgstr "O valor deve ser um inteiro entre 0 e 255."

#: src/web/server/common/pages_voter.ml:781
msgid "Warning:"
msgstr "Atenção:"

#: src/web/clients/tool/tool_js_booth.ml:239
msgid "Warning: the system will accept any integer between 0 and 255 but, according to the election rules, invalid ballots (score too high or candidates not properly ranked) will be rejected at the end of the election."
msgstr ""
"Aviso: o sistema aceitará qualquer valor inteiro entre 0 e 255 mas, de "
"acordo com as regras da eleição, cédulas inválidas (valor muito alto ou "
"candidatos indevidamente classificados) serão rejeitados ao final da eleição."

#: src/web/server/common/pages_common.ml:459
msgid "Warning: this code is valid for 15 minutes, and previous codes sent to this address are no longer valid."
msgstr ""
"Atenção: esse código é válido por 15 minutos e os códigos enviados "
"anteriormente para esse e-mail não são mais válidos."

#: src/web/server/common/pages_voter.ml:158
msgid "We use here the Schulze method and we refer voters to "
msgstr "Nós usamos aqui o método Schulze e nós referimos ao eleitores "

#: src/web/server/common/pages_voter.ml:1200
msgid "When a candidate obtains enough votes to be elected, the votes are transferred to the next candidate in the voter ballot, with a coefficient proportional to the \"surplus\" of votes."
msgstr ""
"Quando o candidato obtém votos suficientes para ser eleito, os votos são "
"transferidos para o próximo candidato na cédula, com o coeficiente "
"proporcional ao \"excedente\" dos votos."

#: src/web/server/common/pages_common.ml:244
msgid "Wish to help with translations?"
msgstr "Gostaria de ajudar com traduções?"

#: src/web/server/common/mails_voter.ml:137
#. src/web/server/common/mails_voter.ml:66
msgid "You are allowed to vote several times."
msgstr "Você pode votar várias vezes."

#: src/web/server/common/mails_voter.ml:116
msgid "You are listed as a voter for the election"
msgstr "Você está registrado(a) como eleitor para essa eleição"

#: src/web/server/common/site_common.ml:109
msgid "You are not allowed to access this page!"
msgstr ""

#: src/web/server/common/pages_common.ml:413
msgid "You can "
msgstr "Você pode "

#: src/web/server/common/pages_common.ml:366
msgid "You can also "
msgstr "Você também pode "

#: src/web/server/common/pages_voter.ml:381
msgid "You can also download the "
msgstr "Você também pode baixar o(a) "

#: src/web/server/common/pages_voter.ml:872
msgid "You can check its presence in the "
msgstr "Você pode verificar sua presença em "

#: src/web/server/common/mails_voter.ml:380
msgid "You can check its presence in the ballot box, accessible at"
msgstr "Você pode verificar sua presença na urna, disponível em"

#: src/web/server/common/pages_common.ml:517
msgid "You cannot log in now. Please try later."
msgstr "Você não pode fazer login agora. Tente novamente mais tarde."

#: src/web/clients/tool/tool_js_booth.ml:127
msgid "You must select at least %d answer(s)"
msgstr "Você deve selecionar pelo menos %d resposta(s)"

#: src/web/clients/tool/tool_js_booth.ml:130
msgid "You must select at most %d answer(s)"
msgstr "Você deve selecionar no máximo %d resposta(s)"

#: src/web/server/common/mails_voter.ml:121
msgid "You will be asked to enter your credential before entering the voting booth."
msgstr ""
"Você será solicitado a inserir sua credencial antes de entrar na cabine de "
"votação."

#: src/web/server/common/mails_voter.ml:120
msgid "You will find below your credential."
msgstr "Você encontrará sua credencial abaixo."

#: src/web/server/common/pages_voter.ml:889
#. src/web/server/common/pages_voter.ml:741
msgid "Your ballot for "
msgstr "Sua cédula para "

#: src/web/server/common/pages_voter.ml:1000
msgid "Your ballot has been encrypted, "
msgstr "Sua cédula foi criptografada, "

#: src/web/server/common/site_voter.ml:115
msgid "Your browser seems to block cookies. Please enable them."
msgstr ""
"Parece que seu navegador está bloqueando os cookies. Por favor habilite-os."

#: src/web/server/common/mails_voter.ml:156
msgid "Your credential for election %s"
msgstr "Sua credencial para eleição %s"

#: src/web/server/common/pages_common.ml:454
msgid "Your e-mail address has been used to authenticate with our Belenios server."
msgstr ""

#: src/web/server/common/mails_voter.ml:84
msgid "Your password for election %s"
msgstr "Sua senha para eleição %s"

#: src/web/server/common/mails_voter.ml:371
msgid "Your smart ballot tracker is"
msgstr "Seu número de rastreio da cédula é"

#: src/web/server/common/pages_voter.ml:1005
#. src/web/server/common/pages_voter.ml:867
#. src/web/server/common/pages_voter.ml:744
msgid "Your smart ballot tracker is "
msgstr "Seu número de rastreio da cédula é "

#: src/web/server/common/mails_voter.ml:361
msgid "Your vote for election"
msgstr "Seu voto para a eleição"

#: src/web/server/common/site_voter.ml:152
msgid "Your vote for election %s"
msgstr "Seu voto para eleição %s"

#: src/web/server/common/pages_voter.ml:783
msgid "Your vote was not recorded!"
msgstr "Seu voto não foi registrado!"

#: src/web/server/common/pages_voter.ml:859
#. src/web/server/common/pages_voter.ml:730
#. src/web/server/common/mails_voter.ml:368
msgid "Your weight is %s."
msgstr ""

#: src/web/server/common/pages_voter.ml:873
msgid "ballot box"
msgstr "urna"

#: src/web/server/common/pages_voter.ml:67
msgid "ballots"
msgstr "cédulas"

#: src/web/server/common/pages_voter.ml:1001
msgid "but not cast yet"
msgstr "mas não foi votado ainda"

#: src/web/server/common/pages_common.ml:369
msgid "change your password"
msgstr "mudar sua senha"

#: src/web/server/common/pages_common.ml:367
msgid "create an account"
msgstr "criar uma conta"

#: src/web/server/common/mails_voter.ml:365
msgid "has been recorded."
msgstr "foi registrado."

#: src/web/clients/tool/tool_js_booth.ml:379
#. src/web/server/common/pages_voter.ml:1203
#. src/web/server/common/pages_voter.ml:1150
msgid "here"
msgstr "aqui"

#: src/web/server/common/pages_voter.ml:1206
msgid "our code of STV"
msgstr "nosso código de STV (VUT)"

#: src/web/server/common/pages_voter.ml:57
msgid "parameters"
msgstr "parâmetros"

#: src/web/server/common/pages_voter.ml:409
msgid "personal data policy"
msgstr "política de privacidade de dados pessoais"

#: src/web/server/common/pages_voter.ml:63
msgid "public credentials"
msgstr "credenciais públicas"

#: src/web/server/common/pages_voter.ml:383
msgid "result with cryptographic proofs"
msgstr "resultado com provas criptográficas"

#: src/web/server/common/web_common.ml:76
msgid "some proofs failed verification"
msgstr "algumas provas falharam na verificação"

#: src/web/server/common/pages_voter.ml:787
msgid "start from the beginning"
msgstr "começar do ínicio"

#: src/web/server/common/pages_voter.ml:159
msgid "the Wikipedia page"
msgstr "página da Wikipedia"

#: src/web/server/common/web_common.ml:72
msgid "the election is closed"
msgstr "A eleição está fechada"

#: src/web/server/common/pages_voter.ml:60
msgid "trustees"
msgstr "autoridades certificadoras"

#: src/web/server/common/pages_common.ml:414
msgid "try to log in again"
msgstr "Tente entrar novamente"

#: src/web/server/common/web_common.ml:78
msgid "you are not allowed to revote"
msgstr "Você não está autorizado(a) a votar novamente"

#: src/web/server/common/web_common.ml:73
msgid "you are not allowed to vote"
msgstr "Você não está autorizado(a) a votar"

#: src/web/server/common/web_common.ml:80
msgid "you are not allowed to vote with this credential"
msgstr "Você não está autorizado a votar com essa credencial"

#: src/web/server/common/web_common.ml:74
msgid "your ballot has a syntax error (%s)"
msgstr "Sua cédula tem um erro de sintaxe (%s)"

#: src/web/server/common/web_common.ml:75
msgid "your ballot is not in canonical form"
msgstr ""

#: src/web/server/common/web_common.ml:81
msgid "your credential has a bad weight"
msgstr "sua credencial tem um mau peso"

#: src/web/server/common/web_common.ml:79
msgid "your credential has already been used"
msgstr "Sua credencial já foi utilizada"

#: src/web/server/common/web_common.ml:77
msgid "your credential is invalid"
msgstr "Sua credencial é inválida"

